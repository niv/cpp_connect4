#include "StdAfx.h"
#include "AI.h"
#include "Board.h"

AI::AI(Board ^board,int pc_player,int level)
{
	_board = board;
	_player = pc_player;
	_level = level;
}


/*-------------------------------------------------*/

#pragma region Evaluate Move functions

unsigned int AI::EvaluateMove(int player,int col, int row){
	unsigned temp;
	unsigned mid = e_Middle(col,row);

	if(player == _player)
		_eval_current = true;
	else
		_eval_current = false;

	temp = e_Win(col,row);
	if(temp!=P_NONE)
		return temp;

	temp = e_2Way3Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_3Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_2Way2Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_2Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	return mid;
}

unsigned int AI::EvaluateMove(int col, int row){
	/* like the previus one , but checking the player by given (col,row) */
	unsigned temp;
	unsigned mid = e_Middle(col,row);
	int player = _board->getAt(col,row);

	if(player == _player)
		_eval_current = true;
	else
		_eval_current = false;

	temp = e_Win(col,row);
	if(temp!=P_NONE)
		return temp;

	temp = e_2Way3Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_3Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_2Way2Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	temp = e_2Coins(col,row);
	if(temp!=P_NONE)
		return temp+mid;

	return mid;
}


int AI::EvaluateMoves(array<char,2> ^moves){
	/* evaluating all the moves given
	 * assuming length is _level and the array is filled up 
	 * NOTICE: it will count twice if moves[,] contain two coins that are near each other , meaning it will evaluate them both making "double score" , although it should be small enugh to be unsignificant */
	int i;
	int sum = 0,temp;
	int col,row,player;
	for(i=0;i<_level;i++){
		col = moves[i,0];
		row = moves[i,1];
		player = _board->getAt(col,row);
		temp = EvaluateMove(player,col,row);
		if(player == _player){
			sum += temp;
		} else { // assuming array is filled , meaning its the second player (pc's enemy)
			sum -= temp;
		}
	}
	return sum;
}

#pragma endregion

/*--------------------  MinMax Functions  -------------------------*/
#pragma region NivMinMax

int AI::NivMin(int player,int *finalBestCol,int *finalBestRow,int depth){
	if(depth==0) return 0;
	int col,row;
	int bestMove = 0;
	int tempMove;
	int bestCol = -1;
	int bestRow = -1;
	int bestVal;
	bool firstTime=true;
	for(col = 0; col<COL; col++){
		row = _board->Droppable(col);
		if(row==-1) // not droppable at that column
			continue;
		_board->Drop(player,col,false);
		tempMove = -(signed int)EvaluateMove(player,col,row);
		if(tempMove <= -P_WIN){
			_board->PickUp(col);
			*finalBestCol = col;
			*finalBestRow = row;
			return tempMove;
		}
		bestVal = NivMax(ENEMY_OF(player),finalBestCol,finalBestRow,depth - 1);
		tempMove += bestVal;
		_board->PickUp(col);

		if(tempMove < bestMove || firstTime==true){ // if its the best move or the first time we are in the loop
			firstTime = false;
			bestCol = col;
			bestRow = row;
			bestMove = tempMove;
		}
	}
	*finalBestCol = bestCol;
	*finalBestRow = bestRow;
	return bestMove;
}
int AI::NivMax(int player,int *finalBestCol,int *finalBestRow,int depth){
	if(depth==0) return 0;
	int col,row;
	int bestMove = 0;
	int tempMove;
	int bestCol = -1;
	int bestRow = -1;
	int bestVal;
	bool firstTime=true;
	for(col = 0; col<COL; col++){
		row = _board->Droppable(col);
		if(row==-1) // not droppable at that column
			continue;
		_board->Drop(player,col,false);
		tempMove = EvaluateMove(player,col,row);
		if(tempMove >= P_WIN){ // check if he wins , then can stop checking other options
			_board->PickUp(col);
			*finalBestCol = col;
			*finalBestRow = row;
			return tempMove;
		}
		bestVal = NivMin(ENEMY_OF(player),finalBestCol,finalBestRow,depth - 1);
		tempMove += bestVal;
		_board->PickUp(col);

		if(tempMove > bestMove || firstTime==true){
			firstTime = false;
			bestCol = col;
			bestRow = row;
			bestMove = tempMove;
		}
	}
	*finalBestCol = bestCol;
	*finalBestRow = bestRow;
	return bestMove;
}

#pragma endregion



int AI::Min(int player,int *finalBestCol,int *finalBestRow,array<char,2> ^moves,int depth,int alpha,int beta){
	/* enemy will take the best eval for him , meaning the worst eval for pc 
	 * we get alpha - smallest value from max that if we find a min that is below it , we will stop searching (because we will return this number or smaller than it , meaining anyway that we wont chose it)
	*/
	if(depth==0){
		return EvaluateMoves(moves);
	}
	int col,row;
	int bestEval = -INFINITY;
	int tempEval;
	int bestCol = -1;
	int bestRow = -1;
	bool firstTime=true;

	for(col = 0; col<COL; col++){
		row = DropIfDroppable(player,col,moves,_level-depth);
		if(row == -1) // if not droppable
			continue;
		tempEval = Max(ENEMY_OF(player),finalBestCol,finalBestRow,moves,depth - 1,alpha,beta);
		PickUp(col,moves,_level-depth);

		if(tempEval < bestEval || firstTime==true){
			firstTime = false;
			bestCol = col;
			bestRow = row;
			bestEval = tempEval;

			#ifdef __WITH_ALPHA_BETA
				if(beta>bestEval)
					beta = bestEval;
				if(alpha>=beta)
					break;
			#endif
		}
	}
	*finalBestCol = bestCol;
	*finalBestRow = bestRow;
	return bestEval;
}
int AI::Max(int player,int *finalBestCol,int *finalBestRow,array<char,2> ^moves,int depth,int alpha,int beta){
	/* similar to Min(..).*/
	if(depth==0){
		return EvaluateMoves(moves);
	}
	int col,row;
	int bestEval = INFINITY;
	int tempEval;
	int bestCol = -1;
	int bestRow = -1;
	bool firstTime=true;

	for(col = 0; col<COL; col++){
		row = DropIfDroppable(player,col,moves,_level-depth);
		if(row == -1) // if not droppable
			continue;
		tempEval = Min(ENEMY_OF(player),finalBestCol,finalBestRow,moves,depth - 1,alpha,beta);
		PickUp(col,moves,_level-depth);

		if(tempEval > bestEval || firstTime==true){
			firstTime = false;
			bestCol = col;
			bestRow = row;
			bestEval = tempEval;
			
			#ifdef __WITH_ALPHA_BETA
				if(alpha < bestEval)
					alpha = bestEval;
				if(alpha>=beta)
					break; // out of loop
			#endif
		}
	}
	*finalBestCol = bestCol;
	*finalBestRow = bestRow;
	return bestEval;
}

int AI::Minmax(int *finalBestCol,int *finalBestRow){
	/*--------------------------------------------------*
	 * Things to notice abuot -NivMax-:
	 * a) this minmax use "infinity" as 99999 means it can return more than that if depth is even 3 , because he can do a good move on depth=3 which gives mark 1000 , and on his next move he wins , earning additional 99999 points which makes it about 100100 or something like that. in c# it cuold be better because thers an inifnity object ther.
	 * b) max return a positive value , min return a negetive value and both value returned have the largest absolute value
	 * c) sometimes it return negetive value just in begining. thats ok , because it depands on the depth .. means that he might calculate the other player's moves 2 times and his own 1 time for total of depth 3 etc ..
	 *---------------------------------------------------*/
	array<char,2> ^moves = gcnew array<char,2>(_level,2);
	return Max(_player,finalBestCol,finalBestRow,moves,_level,-ABINFINITY,ABINFINITY);
	//return NivMax(_player,finalBestCol,finalBestRow,_level);
}



/*---------- Help Functions For Minmax ----------------*/

int AI::DropIfDroppable(int player,int col,array<char,2> ^moves,int i){
	/* if player's coin cant be dropped at given column , return (-1)
	 * else , return the row that the coin has been dropped to 
	 * and updating moves[,] accordingly */
	int row = _board->Droppable(col);
	if(row==-1) // not droppable at that column
		return -1;
	_board->Drop(player,col,false);
	moves[i,0] = col;
	moves[i,1] = row;
	return row;
}

void AI::PickUp(int col,array<char,2> ^moves,int i){
	/* picking up the coin at given column , updating moves[,] as well */
	_board->PickUp(col);
	moves[i,0] = -1;
	moves[i,1] = -1;
}

int AI::checkWin(int col,int row){
	if(_board->getAt(col,row) == _player) // need to know WHOM to check winning by using the global flag
		_eval_current = true;
	else
		_eval_current = false;
	int win = e_Win(col,row);
	_eval_current = true;
	return win;
}


/*-------------------------------------------------*/

#pragma region Evaluation Functions
unsigned int AI::e_Win(int col,int row){
	return e_Sequence(col,row,4,P_WIN,P_NONE,false);
}
unsigned int AI::e_3Coins(int col,int row){
	return e_Sequence(col,row,3,P_3COINS,P_NONE,false);
}
unsigned int AI::e_2Coins(int col,int row){
	return e_Sequence(col,row,2,P_2COINS,P_NONE,false);
}
unsigned int AI::e_2Way3Coins(int col,int row){
	return e_Sequence(col,row,3,P_3COINS,P_2WAY_3COINS,true);
}
unsigned int AI::e_2Way2Coins(int col,int row){
	return e_Sequence(col,row,2,P_2COINS,P_2WAY_2COINS,true);
}
unsigned int AI::e_Middle(int col,int row){
	int check;
	check = COL/2-col;
	if(check>=-1 && check<=1){
		if(check==0) 
			return P_EXACT_MIDDLE;
		return P_MIDDLE;
	}
	return P_NORMAL;
}

#pragma endregion 

/*------------------------------*/

#pragma region Function That Evaluation Function Use

unsigned int AI::e_Sequence(int col,int row,int sq,int points,int points2way,bool check2way){
	int direction = _board->winType(col,row,sq);
	if(direction == 0) return P_NONE;
	int player = _eval_current==true ? _player : (ENEMY_OF(_player));

	// PLAYER1 wins are positive
	if((direction > 0 && player==1) || (direction < 0 && player==2)){
		if(check2way)
			return e_Check_Sequence_2Way(col,row,
						System::Math::Abs(direction) - 1,points2way);
		return points;
	}
	return P_NONE;
}
unsigned int AI::e_Check_Sequence_2Way(int col,int row,int direction,int points){
	int dx = wm[direction,0];
	int dy = wm[direction,1];
	int i;
	array<char,2> ^pointers = gcnew array<char,2>(3+3+1,2); // 4 is the max that it gets but there might be more in a row on certain situation (like winning situation). 3+3+1 is the max , because the "1" says he put a coin between the 3 , thus winning.
	
	int len = pointers->Length / 2;
	for(i=0;i<len;i++){
		pointers[i,0] = -1; // will indicate when to stop
	}
	
	this->_board->Check2Way(col,row,dx,dy,pointers); // notice : pointers wont contain our original (col,row)
	pointers[0,0] = col;
	pointers[0,1] = row;
	int x,y;
	int count = 0;

	for(i=0;i<len , pointers[i,0]!= -1;i++){ // we dont know what are the edges so we are going on all the pointers array
		// notice that we only need to check if there are two empty spaces besides every coin (we'll find max 2)
		x = pointers[i,0] + dx;
		y = pointers[i,1] + dy;
		if(IS_LEGAL(x,y)){
			if(this->_board->getAt(x,y) == PLAYER_NULL)
				count++;
		}
		x = pointers[i,0] - dx;
		y = pointers[i,1] - dy;
		if(IS_LEGAL(x,y)){
			if(this->_board->getAt(x,y) == PLAYER_NULL)
				count++;
		}
		if(count==2) // we found , no need to continue checking
			break; 
	}


	if(count == 2){
		return points;
	}
	return P_NONE;
}

#pragma endregion 

/*-----------------------------------------------*/

