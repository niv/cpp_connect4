#include "StdAfx.h"

using namespace System::Drawing;

void Cell::Init(Graphics ^gr){
	g = gr;
}

void Cell::Draw(int x, int y){
	/* drawing a cell */
	g->FillRectangle(b,x,y,CELL_SIZE,CELL_SIZE);		
}

void Cell::Draw(int player, int x, int y){
	/* drawing a cell with a coin inside. if player==0 then drawing blank */
	if(player==0){
		g->FillRectangle(b,x,y,CELL_SIZE,CELL_SIZE);
		return ;
	}
	Pen ^p = player==PLAYER1 ? p1 : (player==PLAYER2 ? p2 : win);
	Draw(x,y);
	g->DrawEllipse(p,x,y,CELL_CIRCLE_WIDTH,CELL_CIRCLE_HEIGHT);
}

bool Cell::isLegal(int x,int y){
	if(x<BOARD_OFFSET_X || x>BOARD_OFFSET_X+COL*(CELL_SIZE+GAP))
		return false;
	if(y<BOARD_OFFSET_Y || y>BOARD_OFFSET_Y+ROW*(CELL_SIZE+GAP))
		return false;
	return true;
}