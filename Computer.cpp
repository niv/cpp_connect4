#include "StdAfx.h"
#include "Computer.h"

Computer::Computer(Board ^board, int player,int level)
{
	_board = board;
	_player = player; //Player(player);
	_ai = gcnew AI(board, player,level);
}

int Computer::MakeMove(int *finalBestCol,int *finalBestRow){ 
	/*** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ***/
	int best = _ai->Minmax(finalBestCol,finalBestRow);
	_board->Drop(_player,*finalBestCol,true);
	return best;
}
