#pragma once

#include "stdafx.h"
#include "AI.h"

/*------------ Constants -------------*/
#define EASY_AI 1
#define MODERATE_AI 2
#define HARD_AI 3
#define CRAZY_AI 4
/*------------------------------------*/

ref class Computer
{
private:
	int _player; // PLAYER1/2
	AI ^_ai;
	Board ^_board;
public:
	Computer(Board ^board, int player,int level);

	int MakeMove(int *finalBestCol,int *finalBestRow);

};