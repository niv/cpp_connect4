#pragma once

/*
Board is a global class , everyone can use it
*/


#include "stdafx.h"
using namespace System::Drawing;

/*---------- Constants -------------*/
#define PLAYER_NULL (0)
#define PLAYER_HIGHLIGHT (9)
#define PLAYER1 (1)
#define PLAYER2 (2)

#define COL 7
#define ROW 6
#define GAP 5
#define BOARD_OFFSET_X 10
#define BOARD_OFFSET_Y 10

#define X_CELL(i) (BOARD_OFFSET_X+(i)*(CELL_SIZE+GAP))
#define Y_CELL(j) (BOARD_OFFSET_Y+(j)*(CELL_SIZE+GAP))

#define I_INDEX(x) (((x)-BOARD_OFFSET_X)/(CELL_SIZE+GAP)) // return the index of the column that x is at
#define J_INDEX(y) (((y)-BOARD_OFFSET_Y)/(CELL_SIZE+GAP))

#define IS_LEGAL(col,row) ((col)>=0 && (col)<COL && (row)>=0 && (row)<ROW)
/*----------------------------------*/



ref class Board
{
private:
	/* --------- Array of the board ----------
	 * 0: no coin
	 * 1: player1 coin
	 * 2: player2 coin
	 * 3: optional , for color-ing the winner
	 * -----------------------------------------*/
	array<char,2> ^_arr; 

	Graphics ^_g;

private:
	delegate void dScan(array<char,2> ^arr,int i,int j,int *win,System::Object ^extra);

public:
	Board(Graphics ^g);

	int getAt(int col,int row); // (col,row) = (i,j)

	void Draw();
	void Update(int player,int i,int j);
	int Drop(int player,int i,bool draw); // return the row it been dropped
	int Droppable(int col);
	int PickUp(int col); // pick up coin at given column

	/* 0: no one wins
	 * 1,2,3,4: top-left-clockwise win of player 1
	 * -1,-2,-3,-4: top-left-clockwise win of player 2 */
	int winType(int col,int row,int winSearch);
	void highlightWin(int type,int col,int row);

	void Check2Way(int col,int row,int dx,int dy,array<char,2> ^pointers);


private:
	/*-----------------------------------------------------------------------*/
	/*
	IMPORTANT : this use a function to scan selected (col,row) , that continue the same way
	meaning if (col,row) is player1 , then it scan at intervals of [dx,dy] from it , untill it wont meet player1 coins
	NOTICE : whatever ^f do , need to consider that (col,row) are not in the calculation
	meaning in Check2Way(..) it wont add to the array (col,row) itself !! which is top important in case (col,row) is in the edge
	*/
	int ScanBoard(int col,int row,int dx,int dy,dScan ^f,System::Object ^extra);
	/*-----------------------------------------------------------------------*/

	static void $sum(array<char,2> ^arr,int i,int j,int *win,System::Object ^extra);
	static void $highlight(array<char,2> ^arr,int i,int j,int *win,System::Object ^extra);
	static void $2way(array<char,2> ^arr,int i,int j,int *win,Object ^extra);

}; // class
