#pragma once
#include "stdafx.h"

using namespace System::Drawing;

/*---------- Constants -------------*/
#define CELL_SIZE 30
#define CELL_FILL_COLOR (Color::White) 
#define CELL_P1_COLOR (Color::Blue)
#define CELL_P2_COLOR (Color::Red)
#define CELL_WIN_COLOR (Color::Yellow)
#define CELL_CIRCLE_WIDTH (CELL_SIZE)
#define CELL_CIRCLE_HEIGHT (CELL_SIZE)
/*----------------------------------*/

ref class Cell
{
private:
	static Graphics ^g;
	static Brush ^b = gcnew System::Drawing::SolidBrush(CELL_FILL_COLOR);
	static Pen ^p1 = gcnew System::Drawing::Pen(CELL_P1_COLOR,2);
	static Pen ^p2 = gcnew System::Drawing::Pen(CELL_P2_COLOR,2);
	static Pen ^win = gcnew System::Drawing::Pen(CELL_WIN_COLOR,4);

public:
	Cell(void){}; // it isnt needed. i want Cell to be static class
	static void Init(Graphics ^gr);
	static void Draw(int x,int y); // draw no coin
	static void Draw(int player,int x,int y); // draw a coin for given player - 0 or 1
	static bool isLegal(int x,int y);
};