#include "stdafx.h"

Board::Board(Graphics ^g){
	_arr = gcnew array<char,2>(COL,ROW);
	_g = g;
	Cell::Init(_g);
	int i,j;
	for(i=0;i<COL;i++){
		for(j=0;j<ROW;j++){
			_arr[i,j] = 0;
		}
	}
	
	/*_arr[0,ROW-1] = PLAYER1;
	_arr[0,ROW-2] = PLAYER1;
	_arr[0,ROW-3] = PLAYER2;
	_arr[0,ROW-4] = PLAYER2;
	_arr[0,ROW-5] = PLAYER2;*/

	//_arr[1,ROW-1] = PLAYER1;
	/*_arr[2,ROW-1] = PLAYER1;
	_arr[3,ROW-1] = PLAYER1*/;

	/*_arr[COL-1-3,ROW-1] = PLAYER2;
	_arr[COL-2-3,ROW-1] = PLAYER2;
	_arr[COL-3-3,ROW-1] = PLAYER1;*/

	/*_arr[COL-1-3,ROW-1] = PLAYER2;
	_arr[COL-2-3,ROW-2] = PLAYER1;
	_arr[COL-3-3,ROW-2] = PLAYER2;
	_arr[COL-3-3,ROW-3] = PLAYER1;

	_arr[COL-4-3,ROW-1] = PLAYER2;
	_arr[COL-4-3,ROW-2] = PLAYER2;*/

}

/*-------------------------------------------------*/

int Board::getAt(int col,int row){
	return _arr[col,row];
}

/*-------------------------------------------------*/

void Board::Draw(){
	int i,j;
	int player;
	for(i=0;i<COL;i++){
		for(j=0;j<ROW;j++){
			player = _arr[i,j];
			if(player == 0)
				Cell::Draw(X_CELL(i),Y_CELL(j));
			else
				Cell::Draw(player,X_CELL(i),Y_CELL(j));
		}
	}
}

void Board::Update(int player,int i,int j){
	Cell::Draw(player,X_CELL(i),Y_CELL(j));
	_arr[i,j] = player;
}

int Board::Drop(int player,int i,bool draw){
	/* drop a player's coin at certain column down untill it hits a coin or the end of the board
	 * bool draw is if u want to draw the coin or not (false is used in AI class)
	 * return the row that it hits , or (-1) if undroppable */
	int j = Droppable(i);
	if(j==-1) return -1;
	if(draw == true)
		Update(player,i,j);
	else
		_arr[i,j] = player;
	return j;
}

int Board::Droppable(int col){
	if(!IS_LEGAL(col,ROW-1)) // row doesnt interest us
		return -1;
	int row = ROW; // j=0 is the most top , j=COL-1 is the most bottom
	while(row--){
		if(_arr[col,row]==0){
			return row;
		}
	}
	return -1;
}

int Board::PickUp(int col){
	/* pick up a coin (if exist) on specified column , return the row (-1 if not exist) */
	int row = Droppable(col)+1;
	if(row==ROW) return -1; // no coin to pick
	Update(PLAYER_NULL,col,row);
	return row;
}

/*-------------------------------------------------*/

int Board::winType(int col,int row,int winSearch ){
	/* return the type of win if at all at (col,row)
	 * 0: no one wins
	 * 1,2,3,4: top-left-clockwise win of player 1
	 * -1,-2,-3,-4: top-left-clockwise win of player 2
	 * assuming valid col/row
	 */

	if(_arr[col,row] == 0) return 0; // checking if thers a coin in ther at all

	int win1,win2,win3,win4; // counters for the win type (they are the absolute value)
	int player = _arr[col,row]==PLAYER1 ? 1 : -1; // the sign of the player
	System::Object ^garbage;

	win1 = 1 + ScanBoard(col,row,AI::wm[0,0],AI::wm[0,1],gcnew dScan(&$sum),garbage); // 1+ because we already got one coin , at (col,row)
	if(win1>=winSearch) return 1 * player;
	win2 = 1 + ScanBoard(col,row,AI::wm[1,0],AI::wm[1,1],gcnew dScan(&$sum),garbage);
	if(win2>=winSearch) return 2 * player;
	win3 = 1 + ScanBoard(col,row,AI::wm[2,0],AI::wm[2,1],gcnew dScan(&$sum),garbage);
	if(win3>=winSearch) return 3 * player;
	win4 = 1 + ScanBoard(col,row,AI::wm[3,0],AI::wm[3,1],gcnew dScan(&$sum),garbage);
	if(win4>=winSearch) return 4 * player;

	return 0;
}

void Board::highlightWin(int type,int col,int row){
	/* color-ing the given winner by the given (col,row) which can be any of the winning
	 * coins at the given type of win ,which is given at winType(..) 
	 * NOTE: assuming (col,row) is legal , and thers acctualy a win in there */
	int dx,dy;
	dx = dy = -1; // case 1

	switch(System::Math::Abs(type)){
		case 2: dx = 0; break;
		case 3: dx = 1; break;
		case 4: dx = 1; dy = 0; break;
	}

	int garbage;
	System::Object ^garbage2;
	
	ScanBoard(col,row,dx,dy,gcnew dScan(&$highlight),garbage2);
	Board::$highlight(_arr,col,row,&garbage,garbage2); // important to do that AFTER ScanBoard , because it changed the value of (col,row) , and ScanBoard check for this value
}

void Board::Check2Way(int col,int row,int dx,int dy,array<char,2> ^pointers){
	ScanBoard(col,row,dx,dy,gcnew dScan(&$2way),pointers);
}

/*--------------------------------------*/

int Board::ScanBoard(int col,int row,int dx,int dy,dScan ^f,System::Object ^extra){
	/* this function check one direction (ex: diagonal) from given coin (col,row)
	 * (like the main diagonal , for dx=-1 , dy=1)
	 * plus doing a given dScan function */
	int win = 0;
	int t1=1,t2=-1;
	int i,j;

	while(t1!=0 || t2!=0){
		if(t1!=0){ // if t1=0 we dont even need to do the next checkings
			i = col + t1 * dx;
			j = row + t1 * dy;
			if(!IS_LEGAL(i,j) || _arr[i,j] != _arr[col,row])
				t1 = 0;
			if(t1!=0){ // if we didnt stop counting and the previous check yeilds a coin of our player
				f(_arr,i,j,&win,extra);
				t1++;
			}
		}
		if(t2!=0){ // if t1=0 we dont even need to do the next checkings
			i = col + t2 * dx;
			j = row + t2 * dy;
			if(!IS_LEGAL(i,j) || _arr[i,j] != _arr[col,row])
				t2 = 0;
			if(t2!=0){ // if we didnt stop counting and the previous check yeilds a coin of our player
				f(_arr,i,j,&win,extra);
				t2--;
			}
		}
	}
	return win;
}

/*-------------------------------------------------*/

void Board::$sum(array<char,2> ^arr,int i,int j,int *win,System::Object ^extra){
	(*win)++;
}

void Board::$highlight(array<char,2> ^arr,int i,int j,int *win,System::Object ^extra){
	arr[i,j] = PLAYER_HIGHLIGHT;
}

void Board::$2way(array<char,2> ^arr,int i,int j,int *win,Object ^extra){
	static int counter = 1;
	if(*win == 0){ // im using *win as a parameter to say if we started a new session of 2way checking , otherwise , counter will continue to go up nonstop untill index is out of range
		// notice that counter=1 because original (col,row) wont be included , thus saving place for it
		counter = 1;
		*win = 1;
	}
	array<char,2> ^pointers = (array<char,2> ^)extra;
	pointers[counter,0] = i;
	pointers[counter,1] = j;
	counter++;
}