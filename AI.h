#pragma once

#include "stdafx.h"

/*--------------- Flags ---------------*/
#define __WITH_ALPHA_BETA

/*--------------- Constants ---------------*/
#define INFINITY 99999
#define ABINFINITY (INFINITY+1) // bigger than INFINITY , for the alpha-beta
// those are the points given for making a certain move
#define P_WIN 99999 // winning
#define P_3COINS 100 // making three coins in a row
#define P_2COINS 10 // making two coins in a row
#define P_2WAY_3COINS 1000 // making three coins with two uncoined edges (certain win unless enemy win the next move)
#define P_2WAY_2COINS 50 // better than normal 2 coins , give opertunity to win
#define P_EXACT_MIDDLE 3 // special - putting coins excactly at middle gives more oprtunity to win , will be combined with the others
#define P_MIDDLE 2 // special - putting coins in middle gives more oprtunity to win , will be combined with the others
#define P_NORMAL 1 // normal move
#define P_NONE 0 // used when not evaluated (like checking for winning but no one wins)
/*-----------------------------------------*/
#define ENEMY_OF(player) ((player)==PLAYER1 ? PLAYER2 : PLAYER1)
/*-----------------------------------------*/


ref class AI
{

	/*---------------- Movment Matrix -----------------*/
	/* every member is (dx,dy). remember that dy is 0 at the top 
	/* ex: (-1,-1) means main diagonal , both directions */

public:
	static array<char,2> ^wm = gcnew array<char,2>(4,2){{-1,-1},{0,-1},{1,-1},{1,0}};

	/*--------------------------------------------------*/


private:
	int _level;
	Board ^_board;
	int _player; // the player pc - PLAYER1/2
	bool _eval_current; // true if evaluating _player , false elsewise (if evaluating the enemy , human usually)

public:
	AI(Board ^board,int pc_player,int level);
	
	unsigned int EvaluateMove(int player,int col,int row);
	unsigned int EvaluateMove(int col,int row);
	int EvaluateMoves(array<char,2> ^moves);




	/*------------ Minmax Functions --------------*/
public:
	int NivMin(int player,int *finalBestCol,int *finalBestRow,int depth);
	int NivMax(int player,int *finalBestCol,int *finalBestRow,int depth);
	int Min(int player,int *finalBestCol,int *finalBestRow,array<char,2> ^moves,int depth,int alpha,int beta);
	int Max(int player,int *finalBestCol,int *finalBestRow,array<char,2> ^moves,int depth,int alpha,int beta);
	int Minmax(int *finalBestCol,int *finalBestRow);

	int DropIfDroppable(int player,int col,array<char,2> ^moves,int i);
	void PickUp(int col,array<char,2> ^moves,int i);
	int checkWin(int col,int row);

	/*------------ Private Functions for Evaluate --------------*/
private:
	unsigned int e_Win(int col,int row);
	unsigned int e_3Coins(int col,int row);
	unsigned int e_2Coins(int col,int row);
	unsigned int e_2Way3Coins(int col,int row);
	unsigned int e_2Way2Coins(int col,int row);
	unsigned int e_Middle(int col,int row);

	unsigned int e_Sequence(int col,int row,int sq,int points,int points2way,bool check2way);
	unsigned int e_Check_Sequence_2Way(int col,int row,int sq,int points);


};
